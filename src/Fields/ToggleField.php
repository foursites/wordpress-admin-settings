<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-03-31, 11:26:43 am
 * Modified:    2020-08-07, 04:56:36 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class ToggleField extends AbstractField
{
    protected function getHtml()
    {
        ob_start(); ?>
<div class="foursites-wordpress-admin-settings-ui-toggle">
    <input
        type="checkbox"
        id="<?php echo $this->getID(); ?>"
        name="<?php echo $this->getName(); ?>"
        value="toggle_on"
        <?php echo $this->isChecked(); ?>
    >
    <label for="<?php echo $this->getID(); ?>"></label>
    <?php echo $this->getDescriptionHTML(); ?>
</div>
<?php
        return ob_get_clean();
    }

    protected function isChecked()
    {
        return $this->getValue() ? 'checked' : '';
    }
}

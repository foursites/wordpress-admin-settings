<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-04-08, 03:40:09 pm
 * Modified:    2020-08-07, 04:48:48 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class EditorField extends AbstractField
{
    protected function getHtml()
    {
        $settings = [
            'media_buttons'    => false,
            'drag_drop_upload' => false,
            'textarea_rows'    => 4,
            'textarea_name'    => $this->getName(),
            'editor_class'     => implode(' ', $this->getFieldClasses()),
            'teeny'            => true,
        ];
        ob_start();
        wp_editor($this->getValue(), $this->getID(), $settings);
        echo $this->getDescriptionHTML();

        return ob_get_clean();
    }
}

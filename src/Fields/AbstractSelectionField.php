<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-04-01, 03:00:33 pm
 * Modified:    2020-08-07, 04:48:48 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('These are not the droids you are looking for...');

abstract class AbstractSelectionField extends AbstractField
{
    protected $options;

    public function __construct($args)
    {
        parent::__construct($args);

        $this->options = isset($args['options']) ? $args['options'] : [];
    }

    public function getOptions()
    {
        return $this->options;
    }
}

<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-03-31, 04:08:12 pm
 * Modified:    2020-08-07, 04:48:48 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class SelectField extends AbstractSelectionField
{
    protected function getHtml()
    {
        $field_classes = $this->getFieldClasses() ?: [];
        ob_start(); ?>
<select
    name="<?php echo $this->getName(); ?>"
    id="<?php echo $this->getID(); ?>"
    class="<?php echo implode(' ', $field_classes); ?>">
    <?php foreach ($this->getOptions() as $key => $value) : ?>
        <?php $selected = strval($key) === $this->getValue() ? ' selected="selected"' : ''; ?>
        <option value="<?php echo $key; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
    <?php endforeach; ?>
</select>
<?php echo $this->getDescriptionHTML(); ?>
<?php
        return ob_get_clean();
    }

    protected function getChoiceID($key)
    {
        return $this->getID() . '_' . $key;
    }
}

<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-03-30, 04:47:20 pm
 * Modified:    2020-08-07, 04:48:48 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings\Fields;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class TextField extends AbstractField
{
    protected function getHtml()
    {
        $field_classes = $this->getFieldClasses() ?: [ 'regular-text' ];
        ob_start(); ?>
<input
    type="<?php echo $this->getType(); ?>"
    class="<?php echo implode(' ', $field_classes); ?>"
    id="<?php echo $this->getID(); ?>"
    name="<?php echo $this->getName(); ?>"
    value="<?php echo $this->getValue(); ?>"
    placeholder="<?php echo $this->getPlaceholder(); ?>"
>
<?php echo $this->getDescriptionHTML(); ?>
<?php
        return ob_get_clean();
    }
}

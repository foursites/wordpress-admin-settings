<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-08-07, 04:45:26 pm
 * Modified:    2020-08-18, 12:04:37 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class Callbacks
{
    public function sanitizeSettings($settings)
    {
        foreach ($settings as $field_id => $setting) {
            if ('toggle_on' === $setting) {
                $settings[$field_id] = true;
            }
        }

        return $settings;
    }

    public function settingsSectionCallback()
    {
        return;
    }

    public function settingsField($args = [])
    {
        $class = $this->getSettingsFieldClass($args['type']);

        echo new $class($args);
    }

    protected function getSettingsFieldClass($type = null)
    {
        $type = $type ?: 'text';

        if ( class_exists( $type ) ) {
            return $type;
        }

        $classname = ucfirst($type) . 'Field';
        $namespace = 'Foursites\\WordPressAdminSettings\\Fields\\';

        return class_exists($namespace . $classname) ? $namespace . $classname : $namespace . 'TextField';
    }
}

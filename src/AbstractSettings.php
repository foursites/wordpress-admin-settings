<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-07-08, 02:50:19 pm
 * Modified:    2020-08-10, 11:16:12 am
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings;


defined('ABSPATH') or die('These are not the droids you are looking for...');

abstract class AbstractSettings implements SettingsInterface
{
    protected $settings_id;

    public function __construct()
    {
        $this->settings_id = $this->getSettingsID();
    }

    public static function get($field_id, $default = null)
    {
        $instance = new static();

        $options = get_option($instance->settings_id);

        if (!$options || !isset($options[$field_id])) {
            return $default;
        }

        return $options[$field_id];
    }
}

<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-08-10, 08:51:51 am
 * Modified:    2020-08-10, 11:24:43 am
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings;

defined('ABSPATH') or die('These are not the droids you are looking for...');

interface SettingsInterface
{
    public function register();
    public function getSettingsID();
    public function addSettings($settings);
}

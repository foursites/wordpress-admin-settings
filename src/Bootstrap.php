<?php

/**
 * @package:    foursites-wordpress-admin-settings
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-08-07, 04:35:47 pm
 * Modified:    2020-08-11, 03:53:45 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminSettings;

use Foursites\WordPressAdminSettings\Callbacks;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class Bootstrap
{
    protected $callbacks;

    protected $settings = [];

    protected $sections = [];

    protected $fields = [];

    public function __construct()
    {
        $this->callbacks = new Callbacks;
    }

    public function register()
    {
        add_action('admin_menu', [$this, 'addSettings']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueFieldsStyle']);
    }

    public function addSettings()
    {
        $this->build();
        $this->registerSettings();
        $this->registerSections();
        $this->registerFields();
    }

    public function enqueueFieldsStyle()
    {
        wp_enqueue_style(
            'foursites-wordpress-admin-settings',
            trailingslashit( $this->getBaseURL() ) . 'assets/style.css',
            [],
            filemtime(trailingslashit( $this->getBasePath() ) . 'assets/style.css' )
        );
    }

    protected function build()
    {
        foreach ($this->getSettingsFromFilter() as $_setting) {
            $this->addSetting($_setting['id']);
            foreach ($_setting['sections'] as $_section) {
                $section = $this->addSection($_section, $_setting['page']);
                foreach ($_section['fields'] as $_field) {
                    $this->addField($_field, $section, $_setting['id']);
                }
            }
        }
    }

    protected function getSettingsFromFilter()
    {
        $filter = apply_filters('foursites_wordpress_admin_settings_filter_name', 'fs_wp_settings');
        return apply_filters($filter, []);
    }

    protected function addSetting($id)
    {
        $this->settings[] = $id;
    }

    protected function addSection($section, $page)
    {
        $section = [
            'id'      => $section['id'],
            'title'   => $section['title'],
            'callback'=> [ $this->callbacks, 'settingsSectionCallback'],
            'page'    => $page,
        ];
        $this->sections[] = $section;

        return $section;
    }

    protected function addField($field, $section, $option_name)
    {
        $this->fields[] = [
            'id'      => $field['id'],
            'title'   => $field['title'],
            'callback'=> [ $this->callbacks, 'settingsField'],
            'page'    => $section['page'],
            'section' => $section['id'],
            'args'    => [
                'option_name'   => $option_name,
                'label'         => $field['title'],
                'label_for'     => $field['id'],
                'type'          => isset($field['type']) ? $field['type'] : 'text',
                'description'   => isset($field['description']) ? $field['description'] : '',
                'default'       => isset($field['default']) ? $field['default'] : '',
                'options'       => isset($field['options']) ? $field['options'] : [],
                'field_classes' => isset($field['field_classes']) ? $field['field_classes'] : [],
            ],
        ];
    }

    protected function registerSettings()
    {
        foreach ($this->getSettings() as $id) {
            register_setting(
                $id,
                $id,
                [
                    'sanitize_callback' => [$this->callbacks, 'sanitizeSettings'],
                ]
            );
        }
    }

    protected function registerSections()
    {
        foreach ($this->getSections() as $section) {
            add_settings_section(
                $section['id'],
                $section['title'],
                $section['callback'],
                $section['page']
            );
        }
    }

    protected function registerFields()
    {
        foreach ($this->getFields() as $field) {
            add_settings_field(
                $field['id'],
                $field['title'],
                (isset($field['callback']) ? $field['callback'] : []),
                $field['page'],
                $field['section'],
                (isset($field['args']) ? $field['args'] : [])
            );
        }
    }

    protected function getSettings()
    {
        return $this->settings;
    }

    protected function getSections()
    {
        return $this->sections;
    }

    protected function getFields()
    {
        return $this->fields;
    }

    protected function getBasePath()
    {
        return $this->isThemeFile() ? get_template_directory() . '/vendor/foursites-admin-settings' : plugin_dir_path(dirname(__FILE__));
    }

    protected function getBaseURL()
    {
        return $this->isThemeFile() ? get_template_directory_uri() . '/vendor/foursites-admin-settings' : plugin_dir_url(dirname(__FILE__));
    }

    protected function isThemeFile( $path = __FILE__ )
    {
        $root = str_replace( '\\', '/', get_theme_root() );
        $path = str_replace( '\\', '/', $path );
        return stripos( $path, $root ) !== false;
    }
}
